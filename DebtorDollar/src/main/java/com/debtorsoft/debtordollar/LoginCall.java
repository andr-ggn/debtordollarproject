package com.debtorsoft.debtordollar;

import android.os.AsyncTask;
import android.util.Log;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class LoginCall extends AsyncTask<Object, Integer, Long> {

    private AsyncTaskCompleteListener listener;
    private SoapParametersModel params;
    private static final String TAG = "DebtorSoft";
    private String responseString = "";


    public LoginCall(SoapParametersModel params, AsyncTaskCompleteListener listener)
    {
        this.listener = listener;
        this.params = params;
    }


    @Override
    protected Long doInBackground(Object... urls) {
        getHistoryString();
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {

    }

    @Override
    protected void onPostExecute(Long result) {
        listener.onTaskComplete(responseString);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }


    private void getHistoryString() {

        SoapObject request = new SoapObject(params.getNamespace(), "getUser");
        request.addProperty("email", params.getLoginParameter());
        request.addProperty("pass", params.getPasswordParameter());
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.setOutputSoapObject(request);
        soapEnvelope.dotNet = true;
        HttpTransportSE aht = new HttpTransportSE(params.getUrl());
        aht.debug = true;
        // LOGIC! DO NOT TOUCH!!!!!!!!!
        try {
            aht.call(params.getNamespace() + "getUser", soapEnvelope);
            Object response = soapEnvelope.getResponse();
            Log.d(TAG, "the response contains: " + response.toString());
            responseString = response.toString();
        } catch (Exception e) {
            Log.d(TAG, "Exception : " + e.toString());
        } finally {
            Log.d(TAG, "requestDump : " + aht.requestDump);
            Log.d(TAG, "responseDump : " + aht.responseDump);
        }


    }

}
