package com.debtorsoft.debtordollar;

/**
 * Created by droid on 11/8/13.
 */
public class SoapParametersModel {
    String namespace;
    String url;
    String loginParameter;
    String passwordParameter;

    public SoapParametersModel(String namespace, String url, String loginParameter, String passwordParameter) {
        this.namespace = namespace;
        this.url = url;
        this.loginParameter = loginParameter;
        this.passwordParameter = passwordParameter;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setLoginParameter(String loginParameter) {
        this.loginParameter = loginParameter;
    }

    public void setPasswordParameter(String passwordParameter) {
        this.passwordParameter = passwordParameter;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getUrl() {
        return url;
    }

    public String getLoginParameter() {
        return loginParameter;
    }

    public String getPasswordParameter() {
        return passwordParameter;
    }
}
