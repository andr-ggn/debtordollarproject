package com.debtorsoft.debtordollar;


import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class UIController {

    public void setBackground (View v){
        v.setBackgroundResource(R.drawable.bg);
    }

//    public void removeAllFocus (View loginView, View passwordView){
//
//        TextView login =  (TextView) loginView;
//        TextView password =  (TextView) passwordView;
//        login.clearFocus();
//        password.clearFocus();
//    }

    public void testRemoveAllFocus (ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.clearFocus();
            if (child instanceof ViewGroup){
                testRemoveAllFocus((ViewGroup) child);
            }
        }
    } 
    public void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }

    public void invisibleVisibleControls(boolean visible, ViewGroup vg){
        Log.d("DebtorSoft", "invisibleVisibleControls");
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            if(visible == true){
                child.setVisibility(View.VISIBLE);
                Log.d("DebtorSoft", "child.setVisibility(View.VISIBLE);");
            } else {
                child.setVisibility(View.GONE);
                Log.d("DebtorSoft", "child.setVisibility(View.GONE);");
            }
            if (child instanceof ViewGroup){
                invisibleVisibleControls(visible, (ViewGroup)child);
            }
        }
    }

}
