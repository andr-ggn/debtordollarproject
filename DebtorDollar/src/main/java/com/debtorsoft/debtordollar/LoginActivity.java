package com.debtorsoft.debtordollar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;


public class LoginActivity extends FragmentActivity { //ActionBarActivity


    private static final String NAMESPACE = "http://69.162.166.132/";
    private static final String URL = "http://69.162.166.132/Service/Service1.asmx";
    private static final String TAG = "DebtorSoft";
    private UIController uiController = new UIController();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();
        setContentView(R.layout.layout_login);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }




    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager inputMethodManager = (InputMethodManager) getApplication().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//        if ((findViewById(R.id.loginEditView)!=null)&&(findViewById(R.id.loginEditView)!=null)) uiController.removeAllFocus(findViewById(R.id.loginEditView), findViewById(R.id.passwordEditView));

        if (findViewById(R.id.contentRelativeLayout)!=null) uiController.testRemoveAllFocus((ViewGroup) findViewById(R.id.contentRelativeLayout));
        if (findViewById(R.id.contentForgotPasswordLayout)!=null) uiController.testRemoveAllFocus((ViewGroup) findViewById(R.id.contentForgotPasswordLayout));

        return false;
    }


    class PlaceholderFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_login, container, false);

            final EditText loginView = (EditText) rootView.findViewById(R.id.loginEditView);
            final EditText passwordView = (EditText) rootView.findViewById(R.id.passwordEditView);
            Button loginButton = (Button) rootView.findViewById(R.id.loginButton);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uiController.disableEnableControls(false, (ViewGroup)rootView.findViewById(R.id.contentRelativeLayout));
                    uiController.invisibleVisibleControls(true, (ViewGroup)rootView.findViewById(R.id.indicatorRelativeLayout));
//                    uiController.removeAllFocusremoveAllFocusremoveAllFocus(rootView.findViewById(R.id.loginEditView), rootView.findViewById(R.id.passwordEditView));

                    SoapParametersModel params = new SoapParametersModel(NAMESPACE, URL, loginView.getText().toString(), passwordView.getText().toString());
                    new LoginCall(params, new AsyncTaskCompleteListener() {
                        @Override
                        public void onTaskComplete(String result) {
                            Log.d(TAG, result + " ....maybe ok?");
                            uiController.disableEnableControls(true, (ViewGroup)rootView.findViewById(R.id.contentRelativeLayout));
                            uiController.invisibleVisibleControls(false, (ViewGroup)rootView.findViewById(R.id.indicatorRelativeLayout));

                            Intent intent = new Intent(getActivity(), PieChartsActivity.class);
                            getActivity().startActivity(intent);

                        }
                    }).execute();
                }
            });


            final TextView toForgot = (TextView) rootView.findViewById(R.id.toForgot);
            toForgot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_next_in, R.anim.slide_next_out);
                    transaction.replace(R.id.container, new NewFragment());
                    transaction.commit();
                }
            });

            return rootView;
        }




    }

    class NewFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.backText);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_prev_in, R.anim.slide_prev_out);
                    transaction.replace(R.id.container, new PlaceholderFragment());
                    transaction.commit();
                }
            });


            return rootView;
        }
    }


}
